/* Display header */
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdio.h>

#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

class Display
{
public:
    /* Constructor/Deconstructor */
    Display(void);
    virtual ~Display(void);

    /* Create the window */
    bool init(const int w, const int h, const char *title);

    /* Returns glfwWindowShouldClose */
    bool shouldClose(void);
 
    /* Swap buffers and poll events */
    void update(void);

private:
    GLFWwindow *m_window;
};

#endif // DISPLAY_H_INCLUDED

