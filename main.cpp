#include "Display.hpp"
#include "Shader.hpp"
#include "glm/gtc/matrix_transform.hpp"

GLfloat vertices[] = {
    // vertices
   -0.5f, -0.5f, 0.0f,   // bottom left
    0.5f, -0.5f, 0.0f,   // bottom right
    0.5f,  0.5f, 0.0f,   // top right
};

int main(int argc, char *argv[])
{
    Display display;
    Shader shader;

    /* Init the display */
    if (!display.init(640, 480, "Hello GLFW")) return -1;
    
    // test?
    glEnable(GL_DEPTH_TEST);
    
    /* Init the shader */
    shader.init("VertexShader.glsl", "FragmentShader.glsl");
    
    /* Create the Vertex Array Object */
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);
    
    /* Create the vertex buffer */
    GLuint VertexBufferID;
    glGenBuffers(1, &VertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, VertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    /* Create our Mode/View/Projection Matrices */
    glm::mat4 projMat = glm::perspective(
        glm::radians(45.0f), // field of view
        (float) 640.0 / (float) 480.0, // aspect
        0.1f, // near clip
        100.0f // far clip
    );
    glm::mat4 viewMat = glm::lookAt(
        glm::vec3(0,0,2), // camera position
        glm::vec3(0,0,0), // look at pos
        glm::vec3(0,1,0)  // up
    );
    glm::mat4 modelMat = glm::mat4(1.0f); // identity for now (model is at center)
    
    // total transform matrix
    glm::mat4 mvpMat = projMat * viewMat * modelMat;
    
    while (!display.shouldClose())
    {
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        /* Tell opengl to use our shader */
        shader.use();
        
        /* Send data to the shader */
        glUniformMatrix4fv(shader.mvpLoc(), 1, GL_FALSE, &mvpMat[0][0]);
        
        /* Draw the object */
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, VertexBufferID);
        glVertexAttribPointer(
            0,        // layout location for aPos in vertex shader
            3,        // size,
            GL_FLOAT, // type
            GL_FALSE, // normalize?
            0,
            (void*)0
        );
        glDrawArrays(GL_TRIANGLES, 0, 3); // 3 vertices
        glDisableVertexAttribArray(0);
        
        display.update();
    }

    return 0;
}

