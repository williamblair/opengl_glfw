#include "Display.hpp"

Display::Display(void)
{
    m_window = NULL;
}

Display::~Display(void)
{
    glfwTerminate();
}

bool Display::init(const int w, const int h, const char *title)
{
    if (!glfwInit()) return false;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    m_window = glfwCreateWindow(w, h, title, NULL, NULL);
    if (!m_window) return false;
    
    glfwMakeContextCurrent(m_window);

    glewInit();
    printf("%s\n", glGetString(GL_VERSION));

    return true;
}

bool Display::shouldClose(void)
{
    return glfwWindowShouldClose(m_window);
}

void Display::update()
{
    glfwSwapBuffers(m_window);

    glfwPollEvents();
}

