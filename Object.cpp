#include "Object.hpp"

/* Constructor */
Object::Object(const GLfloat *vertices, const GLint vertSize,
               const GLuint *indices,  const GLint indSize,
               bool hasColors)
{
    this->init(vertices, vertSize, indices, indSize, hasColors);
}

Object::Object(void)
{
    m_vertices = NULL;
    m_indices = NULL;
}

/* Deconstructor */
Object::~Object(void)
{
    if (m_vertices) {
        free(m_vertices);
        m_vertices = NULL;
    }
    if (m_indices) {
        free(m_indices);
        m_indices = NULL;
    }
}

bool Object::init(const GLfloat *vertices, const GLint vertSize,
               const GLuint *indices,  const GLint indSize,
               bool hasColors)
{
    /* Initialize variables */
    m_vertices = m_indices = NULL;
    m_vbo = m_vao = m_ebo = 0;
    m_hasColors = hasColors;

    /* Allocate memory */
    if (m_vertices) {
        free(m_vertices);
    }
    if (m_indices) {
        free(m_indices);
    }
    m_vertices = m_indices = NULL;
    m_vertices = (GLfloat *) malloc(vertSize);
    if (!m_vertices) {
        std::cerr << "Object: failed to allocate memory for vertices\n";
        return false;
    }

    /* Copy the given array into memory */
    memcpy(m_vertices, vertices, vertSize); 

    /* Copy the size */
    m_verticesSize = vertSize;
    m_numVertices  = vertSize / sizeof(GLfloat);
    m_numPoints   = m_numVertices / 3;

    /* Allocate memory for the indices */
    m_indices = (GLfloat *) malloc(indSize);
    if (!m_indices) {
        std::cerr << "Object: failed to allocate memory for indices\n";
        return false;
    }

    /* Copy the given array into memory */
    memcpy(m_indices, indices, indSize); 

    /* Copy the size */
    m_indicesSize = indSize;
    m_numIndices  = indSize / sizeof(GLfloat);

    /* Init vao and vbo */
    if (!m_createVertexObjects()) return false;

    return true;
}


bool Object::m_createVertexObjects(void)
{
    /* Generate objects */
    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &m_vbo);
    glGenBuffers(1, &m_ebo);

    /* Bind */
    glBindVertexArray(m_vao);

    /* Create the vertex buffer object and element buffer object */
    m_createVBO();
    m_createEBO();

    /* Set vertex attributes */
    int stride = (m_hasColors) ? 6 : 3;    

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride * sizeof(GLfloat), 0);
    glEnableVertexAttribArray(0);

    if (m_hasColors) {
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);
    }    

    /* Unbind */
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return true;
}

void Object::m_createVBO(void)
{
    /* Bind */
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    /* Apply buffer data */
    glBufferData(GL_ARRAY_BUFFER, m_verticesSize, m_vertices, GL_STATIC_DRAW);
}

void Object::m_createEBO(void)
{
    /* Bind */
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);

    /* Apply buffer data */
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indicesSize, m_indices, GL_STATIC_DRAW);

}

void Object::draw(void)
{
    /* Bind buffer */
    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);

    /* Draw elements */
    //glDrawArrays(GL_TRIANGLES, 0, m_numPoints);
    glDrawElements(GL_TRIANGLES, m_numIndices, GL_UNSIGNED_INT, 0);

    /* Unbind buffer */
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


