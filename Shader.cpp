#include "Shader.hpp"


Shader::Shader(void)
{}

Shader::~Shader(void)
{}

Shader::Shader(const std::string vertexFile, const std::string fragFile)
{
    loadShaders(vertexFile, fragFile);
}

bool Shader::init(const std::string vertexFile, const std::string fragFile)
{
    return loadShaders(vertexFile, fragFile);
}


bool Shader::loadShaders(const std::string vertexFile, const std::string fragFile)
{
    // get the string data of both files
    std::string vFileStr = loadShaderStr(vertexFile);
    std::string fFileStr = loadShaderStr(fragFile);

    // initialze each shader
    GLuint vertexShaderID   = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    const char *shaderSrc = vFileStr.c_str();

    // send and compile the vertex shader
    glShaderSource(vertexShaderID, 1, &shaderSrc, NULL);
    glCompileShader(vertexShaderID);

    // check for success
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertexShaderID, 512, NULL, infoLog);
        std::cerr << "Shader::Shader: Vertex Shader Compilation failed: "
            << infoLog << std::endl;
        exit(EXIT_FAILURE);
    }

    shaderSrc = fFileStr.c_str();
    
    // send and compile the fragment shader
    glShaderSource(fragmentShaderID, 1, &shaderSrc, NULL);
    glCompileShader(fragmentShaderID);

    // check for success
    glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragmentShaderID, 512, NULL, infoLog);
        std::cerr << "Shader::Shader: Fragment Shader Compilation failed: "
            << infoLog << std::endl;
        exit(EXIT_FAILURE);
    }

    // link the shaders to the main program
    m_progID = glCreateProgram();
    glAttachShader(m_progID, vertexShaderID);
    glAttachShader(m_progID, fragmentShaderID);
    glLinkProgram(m_progID);

    // check for success
    glGetShaderiv(m_progID, GL_LINK_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(m_progID, 512, NULL, infoLog);
        std::cerr << "Shader::Shader: Program Linkage Failed: "
            << infoLog << std::endl;
        exit(EXIT_FAILURE);
    }

    // delete our component shaders now
    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);
    
    return true;
}

std::string Shader::loadShaderStr(const std::string fileName)
{
    std::ifstream f(fileName, std::ios::in);
    if(!f.is_open()) {
        std::cerr << "Shader::getShaderStr: failed to open " << fileName << std::endl;
        return "";
    }

    std::string str((std::istreambuf_iterator<char>(f)),
                 std::istreambuf_iterator<char>());

    return str;
}

void Shader::use(void)
{
    glUseProgram(m_progID);
}


GLuint Shader::programID(void)
{
    return m_progID;
}

GLuint Shader::mvpLoc(void)
{
    return glGetUniformLocation(m_progID, "mvpMat");
}