#ifndef SHADER_H_INCLUDED
#define SHADER_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>

#include <GL/glew.h>

class Shader
{
public:
    Shader(const std::string vertexFile, const std::string fragFile);
    Shader(void);
    ~Shader(void);
    
    // args - file names for the vertex and fragment shader
    bool init(const std::string vertexFile, const std::string fragFile);
    
    // tells opengl to use this shader
    void use(void);
    
    // getters
    GLuint programID(void);
    GLuint mvpLoc(void); // mvp matrix uniform location
    
private:
    bool loadShaders(const std::string vertexFile, const std::string fragFile);
    std::string loadShaderStr(const std::string fileName);
    
    GLuint m_progID; // shader program id
};

#endif
