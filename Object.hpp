#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <cstring>
#include <string>

#ifndef OBJECT_H_INCLUDED
#define OBJECT_H_INCLUDED

class Object
{
public:
    /* Constructor/deconstructor */
    /* size is the number of bytes in the array (For malloc) */
    Object(const GLfloat *vertices, const GLint vertSize,
           const GLuint *indices, const GLint indSize,
           bool hasColors = false);
    Object(void);
    ~Object(void);   

    /* Initialize the object */
    bool init(const GLfloat *vertices, const GLint size,
           const GLuint *indices, const GLint indSize,
           bool hasColors = false);

    /* Bind buffer and draw arrays */
    void draw(void);
 
private:
    GLfloat     *m_vertices;
    GLint        m_numVertices;  // total number of floats
    GLint        m_numPoints;    // total number of 3d points
    GLint        m_verticesSize; // number of bytes in the array

    GLfloat     *m_indices;
    GLint        m_numIndices;   // total number of floats
    GLint        m_indicesSize;  // number of bytes in the array

    bool         m_hasColors;    // wether or not the given array has color data

    /* Vertex buffer and array objects */
    unsigned int m_vbo; // vertex buffer object
    unsigned int m_vao; // vertex array object
    unsigned int m_ebo; // element buffer object

    unsigned int m_cao; // color array object
    unsigned int m_cbo; // color buffer object

    /* Create the array and buffer objects */
    bool m_createVertexObjects(void);

    /* Create the color objects */
    bool m_createColorObjects(void);
    
    /* Create vertex and element buffer objects */
    void m_createVBO(void);
    void m_createEBO(void);
};

#endif // OBJECT_H_INCLUDED

